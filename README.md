### www-gitlab-jp

これは https://www.gitlab.jp/ のソースです。

GitLab日本語公式情報サイトは[GitLab公式ページ](https://about.gitlab.jp)の[過去のソース](https://gitlab.com/gitlab-com/www-gitlab-com/commits/972f68328c1a7593e65cce76bc9630e33d97b44d)をベースに作成しています。

### 開発環境

#### 必要条件

- Ruby 2.5.1
- Bundler
- [Middleman](https://middlemanapp.com/jp/)

#### インストール

macOSにはRubyとRubyGemsの両方がパッケージされていますが、Middlemanの依存ライブラリの一部はインストール時にコンパイルする必要があります。macOSではコンパイルにXcodeのCommand Line Toolsが必要です。Xcodeがインストールされている場合Terminalから実行してください:

```
$ xcode-select --install
```

続いて、以下のコマンドを実行して下さい。`<path-to-repository>`はリポジトリをcloneしたパスです。各自の環境に合わせて変更して下さい。

```
$ cd <path-to-repository>
$ bundle install
```

#### ローカルプレビュー

下記コマンドで Middleman のサーバーが起動します。

```
$ bundle exec middleman
```

Middleman のサーバーが起動したら、ブラウザで http://localhost:4567 にアクセスするとサイトのローカルプレビューを表示できます。

#### ビルド

下記コマンドで本番環境用のファイルをビルドできます。
ビルド結果は `public` ディレクトリに出力されます。

```
$ bundle exec middleman build
```
