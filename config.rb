# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = 'last 2 versions'
end

activate :blog do |blog|
  blog.sources = 'posts/{year}-{month}-{day}-{title}.html'
  blog.permalink = '{year}/{month}/{day}/{title}/index.html'
  blog.layout = 'post'
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_BUILD_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  # blog.custom_collections = {
  #   categories: {
  #     link: '/blog/categories/{categories}/index.html',
  #     template: '/category.html'
  #   }
  # }
  # blog.tag_template = '/templates/tag.html'
  # blog.taglink = '/blog/tags/{tag}/index.html'
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

configure :build do
  set :build_dir, 'public'
  set :base_url, '/www-gitlab-jp' # baseurl for GitLab Pages (project name) - leave empty if you're building a user/group website
  activate :relative_assets # Use relative URLs
  activate :minify_css
  activate :minify_javascript
  activate :minify_html
end

configure :development do
  activate :livereload
end

ignore '**/.keep'
ignore 'includes/*'
ignore '/.*\.swp/'
