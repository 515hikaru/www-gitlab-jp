### サーバー証明書の更新方法

https://www.gitlab.jp/ は Let's Encrypt を使用してサイトをHTTPS化しています。
そのため、サイトのサーバー証明書と秘密鍵を3ヶ月毎に更新する必要あります。

サーバー証明書と秘密鍵を更新するには以下のコマンドを実行してください。

```
$ sudo certbot certonly --force-renewal --manual -d www.gitlab.jp

Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator manual, Installer None
Obtaining a new certificate
Performing the following challenges:
http-01 challenge for www.gitlab.jp

-------------------------------------------------------------------------------
NOTE: The IP of this machine will be publicly logged as having requested this
certificate. If you're running certbot in manual mode on a machine that is not
your server, please ensure you're okay with that.

Are you OK with your IP being logged?
-------------------------------------------------------------------------------
(Y)es/(N)o:
```

IPアドレスが記録することの同意を求められるので `Y` で同意する必要があります。

```
-------------------------------------------------------------------------------
Create a file containing just this data:

70whCjs1dd69ndX9ImIadlQaFw9E5OvVOx1mfpSRkMs.iULZmFdxemv5vKCSPuSsLBRtBMhKIDXvuh6h3368BAA

And make it available on your web server at this URL:

http://www.gitlab.jp/.well-known/acme-challenge/70whCjs1dd69ndX9ImIadlQaFw9E5OvVOx1mfpSRkMs

-------------------------------------------------------------------------------
Press Enter to Continue
```

サイトの所有者であることを検証するために、指定のアドレスから指定のレスポンスを返す必要があります。
そのため、 **Enter の入力待ちを維持したまま**  !72 のように変更してください。
変更が `master` ブランチにマージされると本番環境に自動デプロイされるので、デプロイが完了するまでしばらく待ちます。
指定のアドレスにアクセスして指定のレスポンスが返されることを確認したら、ターミナルに戻りEnterを入力してください。

```
Waiting for verification...
Cleaning up challenges

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/www.gitlab.jp/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/www.gitlab.jp/privkey.pem
   Your cert will expire on 2018-10-01. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```

更新後のサーバー証明書が `/etc/letsencrypt/live/www.gitlab.jp/fullchain.pem` に、秘密鍵が `/etc/letsencrypt/live/www.gitlab.jp/privkey.pem` にそれぞれ保存されます。

続いて、 GitLab の画面で以下の操作を行ってください。

1. Settings > Pages をクリック
1. https://www.gitlab.jp の Details をクリック
1. Edit をクリック
1. Certificate (PEM) に更新後のサーバー証明書を貼り付ける
1. Key (PEM) に更新後の秘密鍵を貼り付ける
1. Save Challenges をクリック
