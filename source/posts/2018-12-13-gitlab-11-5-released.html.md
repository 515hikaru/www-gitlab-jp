---
release_number: "11.5"
title: "GitLab 11.5がリリース、グループセキュリティ、および運用ダッシュボードやPagesのアクセスコントロール"
author: Hiroyuki Sato
author_gitlab: hiroponz
author_twitter: hiroponz79
image_title: '/images/11_5/11_5-cover-image.jpg'
description: "GitLab 11.5がリリースされました。グループセキュリティ、および運用ダッシュボードの追加やGitLab PagesのアクセスコントロールやKubernetes用のKnativeサポートなど様々な点が改善されています。"
twitter_image: '/images/tweets/gitlab-11-5-released.png'
categories: releases
layout: release
header_layout_dark: true
featured: yes
---

## セキュリティチーム用のダッシュボード
{: .intro-header}

長い間、GitLabは開発者がコードをよりセキュアにするために使用されてきました。
しかしこれからは、セキュリティチームがアプリケーションのセキュリティを高め、コンプライアンスの遵守を徹底するためにも利用できます。
11.5では、セキュリティ要員にとって必要な情報が[グループセキュリティダッシュボード](#group-security-dashboard)に集約されます。
これは、最高情報セキュリティ責任者、最高情報責任者、およびセキュリティリーダーにとって特に役立ちます。
グループダッシュボードのデザインが刷新され、複数のプロジェクトにまたがるセキュリティ情報を同時に表示できるようになりました。
これにより、セキュリティの問題を高い視点から俯瞰し、必要に応じて詳細なレポートを表示することができます。
11.5では、静的セキュリティテストの情報を表示することからはじめて、将来的にはより多くのセキュリティ情報をダッシュボードに表示する予定です。

## 運用チーム用のダッシュボード
{: .intro-header}

セキュリティチーム用のグループキュリティダッシュボードと同様に、運用チーム用の[運用ダッシュボード](#operations-dashboard)が提供されます。
これはインスタンスレベルのダッシュボードで、パイプラインの実行やアラートの有無など、プロジェクトの運用状況の全体像を提供します。

## GitLab Pagesへのアクセスコントロール
{: .intro-header}

[GitLab Pages](https://about.gitlab.com/product/pages/)を使用すると、静的なウェブページを簡単にホストできます。
特に、プロジェクトのドキュメントを共有する場合に便利です。
しかし、プロジェクトメンバーだけで共有したいドキュメントの場合はどうすれば良いでしょうか？
これまでは、GitLab Pagesを利用するにはウェブページを全員に公開する必要があり、
それができない場合は、利用をあきらめるしかありませんでした。

GitLab 11.5では、課題やコードと同じように、GitLab Pagesでホストされている静的なウェブページへのアクセスを制御することができます。
権限のないユーザーがページを開くと404(ページが見つかりません)のレスポンスが返されます。
現状では、[GitLab Pagesへのアクセスコントロール](#access-control-for-pages)はセルフホストのGitLabでのみ利用可能ですが、
近い将来GitLab.comでも利用可能となる予定です。

この素晴らしい機能が、オープンソースコミュニティから提供されたことを非常に誇らしく思います。
Pagesへのアクセスコントロールは[コミュニティからの要望が最も多い機能の一つ](https://gitlab.com/gitlab-org/gitlab-ce/issues/33422)でしたが、
[コミュニティからの貢献](https://gitlab.com/gitlab-org/gitlab-pages/merge_requests/94)によって実現されました。

## Kubernetes用のKnative
{: .intro-header}

「サーバーレス」は人気のある、しかし、よく誤解される言葉です。
多くの人がサーバーレスを「サービスとしての関数」やFaaSと同じものと考えていますが、これは[正確ではありません](https://martinfowler.com/articles/serverless.html)。
簡潔に言うと、サーバーレスとはデプロイされているインフラを意識せずに、ソフトウェアを実行可能にするプログラミングのパラダイムです。
したがって、関数に限らずアプリケーションもサーバーレスにすることが可能です。

[Knative](https://cloud.google.com/knative/)は、ビルドやデプロイやモダンなサーバーレスワークロードを管理するためのKubernetesベースのプラットフォームです。
GitLab 11.5では、[Knativeを簡単にデプロイしてGitLabと統合](#easily-deploy-and-integrate-knative-with-gitlab)できるようになりました。
わずか1クリックで、あなたの[接続しているKubernetesクラスタ](https://about.gitlab.com/solutions/kubernetes/)にKnativeをインストールできます。
GitLab 11.5では、あなたのサーバーレスアプリケーションのためにKnativeを使用できるようになりました。
さらに、[11.6ではサーバーレス関数](https://gitlab.com/gitlab-org/gitlab-ce/issues/43959)への対応が予定されています。

現在のところKnativeはアルファ版ですが、Knativeを使用してアプリケーションをデプロイすることで、いくつかの便利な機能を機能を利用できます。
例えば、Knativeは追加の設定を行わずに、podの自動スケールアップ、およびスケールダウンを管理します。
さらに、Knative組み込みのイベントシステムを利用すると、producerとconsumerのプロセス間通信をより簡単に管理でき、マイクロサービスのデプロイに役立ちます。

## その他の改善
{: .intro-header}

このリリースには、その他にも多くの素晴らしい機能が含まれています。
[パイプラインを高速化するためのparallel属性](#parallel-attribute-for-faster-pipelines)、
[課題ボードのカードデザインの変更](#issue-board-cards-redesigned)、
[Jaeger連携](#open-jaeger-from-gitlab)のようなワクワクする機能を見逃さないでください。
さらに、[マージリクエストで変更のない行へのコメント](#comment-on-unchanged-lines-in-merge-request)、
[マージリクエストのレビューのプレビュー](#preview-merge-request-review-before-submitting-it)、
[コード所有者にもとづく承認者の割り当て](#assign-approvers-based-on-code-owners)、
[Review Appのダイレクトリンク](#review-app-direct-link)によって、
コードレビューがより便利に、大きく改善しています。
ぜひ、このリリースに含まれるすべての機能をご確認ください。
